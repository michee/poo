<?php

class Voiture
{
    // pour cacher la marque au public
    private $marque;
    // public pour rendre accessible à tous
    private $couleur;
    public $nbrRoues = 4;


    public function demarrer()
    {
        echo "<p> La voiture démarre </p>";
    }

    // avec la méthode echo
    public function afficherCouleur()
    {
        echo "<p> La voiture est de couleur " . $this->couleur . "</p>";
    }
    // avec la méthode return
    public function afficherMarque()
    {
        return "<p> La Marque est " . $this->marque . "</p>";
    }

    // Pour obtenir la marque quand elle est en private
    public function getMarque(): string
    {
        return $this->marque;
    }

    // Pour definir la marque
    public function setMarque(string $marque): void
    {
        $this->marque = $marque;
    }

    public function getCouleur(): string
    {
        return $this->couleur;
    }
    public function setCouleur(string $couleur): void
    {
        $this->couleur = $couleur;
    }

    public function __construct(string $couleur)
    {    
        $this->couleur = $couleur;
    }
}
