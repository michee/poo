<?php 
require_once './classes/Voiture.php';


$voiture1 = new Voiture("orange");
// $voiture1 ->couleur = "rouge";
// $voiture1 ->marque = "BMW";
// $voiture1 ->couleur = "vert";

// avec la méthode écho tilisation direct de la fonction sans laire de variable avant comme dans l'exemple de la voiture2
$voiture1 ->afficherCouleur();
// utilisation de set 
$voiture1 ->setMarque('BMW ');
echo $voiture1->getMarque();

$voiture1 ->setCouleur('bleu');
echo $voiture1->getCouleur();



$voiture2 = new Voiture("rouge");
// $voiture2 ->couleur ="jaune";
// $voiture2 ->marque ="Mercedes";
$voiture2 ->afficherCouleur();
// $voiture2 ->demarrer();
// avec la méthode return faut d'abord faire un vatiable qui est égale afficher marque de la voiture désirée
// $marqueVoiture = $voiture2 ->afficherMarque();

// $voiture2 ->afficherCouleur();
// echo $marqueVoiture;

$voiture2 ->setMarque('Ford ');
echo $voiture2->getMarque();

$voiture2 ->setCouleur('rose');
echo $voiture2->getCouleur();
echo "<br/>" ;
echo "<br/>" ;
var_dump($voiture1);
echo "<br/>" ;
var_dump($voiture2);
?>
